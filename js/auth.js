var login = true;

const formTemplate =
    '<div class="col-md-12 loginPanel">\n' +
        '<div style="display: flex; justify-content: space-around; width: 100%; padding-bottom: 10px">' +
        '   <div class="col-xs-6">' +
        '   <a class="active" id="goToLogin" onclick="goToLogin()">Log In</a>' +
        '   </div><div class="col-xs-6">' +
        '        <a id="goToReg" onclick="goToRegister()">Register</a>' +
        '    </div>' +
        '</div><hr/>' +
    '    <form id="loginForm" class="form-group">\n' +
    '        <div class="form-group">' +
    '           <input id="loginEmail" class="form-control" type="email" placeholder="email address">\n' +
    '        </div>' +
    '        <div class="form-group">' +
    '           <input id="loginPassword" class="form-control" type="password" placeholder="password">\n' +
    '        </div>' +
    '        <div class="form-group text-center">'+
    '           <div class="remMe">'+
    '               <input type="checkbox" tabindex="3" class="" name="remember" id="remember">'+
    '               <label for="remember"> Remember Me</label>'+
    '           </div>'+
    '       </div>'+
    '    </form>\n' +
    '    <form id="registerForm" class="form-group" style="display: none">\n' +
    '        <div class="form-group">' +
    '           <input id="regUsername" class="form-control" type="text" placeholder="username">\n' +
    '        </div>' +
    '        <div class="form-group">' +
    '           <input id="regEmail" class="form-control" type="email" placeholder="email address">\n' +
    '        </div>' +
    '        <div class="form-group">' +
    '           <input id="regPassword" class="form-control" type="password" placeholder="password">\n' +
    '        </div>' +
    '    </form>\n' +
    '</div>';


function openAuthForm() {

    login = true;

    swal({
        html: formTemplate,
        showCancelButton: true,
        cancelButtonText: "cancel",
        confirmButtonText: "<span id='confirmBtn'>Log in</span>",
        allowOutsideClick: false,
        preConfirm: function () {
            return new Promise(function (resolve, reject) {

                var authData = {};
                var url;

                authData.logout = false;

                if(login) {
                    url = "ajax/loginHandler.php";

                    const emailInp = $("#loginEmail");
                    const passwordInp = $("#loginPassword");

                    authData.email = emailInp.val();
                    authData.password = passwordInp.val();
                } else {
                    url = "ajax/registerHandler.php";

                    const usernameInp = $("#regUsername");
                    const emailInp = $("#regEmail");
                    const passwordInp = $("#regPassword");

                    authData.username = usernameInp.val();
                    authData.email = emailInp.val();
                    authData.password = passwordInp.val();
                }

                $.ajax({
                    url: url,
                    method: "POST",
                    data: {authData: JSON.stringify(authData)}
                }).done(function (response) {
                    console.log(response);
                    response = JSON.parse(response);

                    if (response.type === "success") {
                        swal({
                            title: response.title,
                            text: response.text,
                            type: response.type
                        }).then(function(){
                            window.location.reload();
                        }).catch(swal.noop);
                    } else {
                        reject(response.title + ", " + response.text);
                        $("#swal2-validationerror").delay(2000).fadeOut(200);
                    }
                });

            })
        }
    }).catch(swal.noop);

}

function logout(){
    swal({
        type: "question",
        text: "Are you sure you want to log out?",
        showCancelButton: true,
        confirmButtonText: "yes",
        cancelButtonText: "no",
        preConfirm: function(){
            return new Promise(function(resolve, reject){

                var authData = {};
                authData.logout = true;

                $.ajax({
                    url: "ajax/loginHandler.php",
                    method: "POST",
                    data: {authData: JSON.stringify(authData)}
                }).done(function (response) {
                    console.log(response);
                    response = JSON.parse(response);
                    swal({
                        type: response.type,
                        title: response.title,
                        text: response.text
                    }).then(function(){
                        window.location.reload();
                    }).catch(swal.noop);
                })
            });
        }
    }).catch(swal.noop);
}

function goToLogin() {
    $("#loginForm").delay(290).fadeIn(300);
    $("#registerForm").fadeOut(300);
    $('#goToReg').delay(250).removeClass('active');
    $("#goToLogin").delay(250).addClass('active');
    $("#confirmBtn").html("Log in");
    
    login = true;
}

function goToRegister() {
    $("#registerForm").delay(290).fadeIn(300);
    $("#loginForm").fadeOut(300);
    $('#goToLogin').delay(250).removeClass('active');
    $("#goToReg").delay(250).addClass('active');
    $("#confirmBtn").html("Register");

    login = false;
}

function validateEmail(email) {

}

function validatePassword(password) {

}