var action = "create";
var categories = [];
var allCategoryProperties = [];
var groupsAdded = false;

const quizHandlerUrl = "ajax/quizHandler.php";

$("#timeLimitTip").tooltip();
$("div#imageUpload").dropzone({ url: "/file/post" });

Dropzone.options.myAwesomeDropzone = {
    paramName: "file", // The name that will be used to transfer the file
    maxFilesize: 4, // MB
    addRemoveLinks: true,
    accept: function(file, done) {
        console.log("uploaded");
        done();
    },
    init: function() {
        this.on("thumbnail", function(file, dataUrl) {
            $('.dz-image').last().find('img').attr({width: '100%', height: '100%'});
        });
        this.on("success", function(file) {
            $('.dz-image').css({"width":"100%", "height":"auto"});
        });
        this.on("addedfile", function() {
            if (this.files[1] != null){
                this.removeFile(this.files[0]);
            }
        });
    }
};

$(document).ready(function(){
    getCategories();
    checkAction();
});

function checkAction() {
    if(getUrlParameter("action") === "edit"){
        $("#headline").html("Edit Quiz");
        $("#createQuiz").html("Save Changes!");

        action = "edit_quiz";
        const id = parseInt(getUrlParameter("qId"));

        $.ajax({
            url: quizHandlerUrl,
            method: "GET",
            data: {
                action: action,
                id: id
            }
        }).done(function(response){
            console.log(response);
            response = JSON.parse(response);

            $("#quizName").val(response.name);
            $("#quizDesc").val(response.description);
            $("#visibilitySelect").val(response.status_name);
            $("#languageSelect").val(response.language_name);
            setCategoriesFromId(response.sub_category_id);

            if(response.time_limit === 0){
                $("#timeLimitBool").prop("checked", false);
            } else {
                var timeLimitBool = $("#timeLimitBool");
                timeLimitBool.prop("checked", true);
                toggleTimeInps(timeLimitBool);
                $("#fullTimeInp").val(response.time_limit);
            }
        })
    }
}

function getCategories(){
    $.ajax({
        url: quizHandlerUrl,
        method: "GET",
        data: { action: "get_categories" }
    }).done(function(response){
        console.log(response);
        response = JSON.parse(response);

        categories = response;
        allCategoryProperties = Object.keys(categories);

        //adding main categories;
        for(var i = 0; i < categories.main_categories.length; i++){
            var option = document.createElement("option");
            $(option).attr("value", categories.main_categories[i].name);
            $(option).html(categories.main_categories[i].name);
            $("#mainCategorySelect").append(option);
        }
        //adding sub-categories for first category;
        console.log(categories.main_categories[0].name);
        addSubCategoryArray(categories.main_categories[0].name);

    });
}

function getGroups(){
    if(!groupsAdded) {

        var groupSelect = $("#groupSelect");

        $.ajax({
            url: "ajax/userInfoHandler.php",
            method: "GET",
            data: {action: "get_groups"}
        }).done(function (response) {
            console.log(response);
            response = JSON.parse(response);

            for (var i = 0; i < response.length; i++) {
                var option = document.createElement("option");
                $(option).data("id", response[i].id);
                $(option).attr("id", "s_" + response[i].name.split(" ")[0]);
                $(option).attr("value", response[i].name);
                $(option).html(response[i].name);
                groupSelect.append(option);
            }

            var createOption = document.createElement("option");
            $(createOption).html("Create new group");
            $(groupSelect).append(createOption);
            $(groupSelect).val([]);

            groupsAdded = true;
        });
    }
}

function addSubCategoryArray(categoryName){
    var subCategorySelect = $("#subCategorySelect");
    subCategorySelect.empty();

    for(var i = 1; i < allCategoryProperties.length; i++){
        console.log(allCategoryProperties[i] + ", " + categoryName);
        if(allCategoryProperties[i] === categoryName){
            var subCategoryArray = categories[allCategoryProperties[i]];

            for(var j = 0; j < subCategoryArray.length; j++){
                var option = document.createElement("option");
                $(option).data("id", subCategoryArray[j].id);
                $(option).attr("id", "s_" + subCategoryArray[j].name.split(" ")[0]);
                $(option).attr("value", subCategoryArray[j].name);
                $(option).html(subCategoryArray[j].name);

                subCategorySelect.append(option);
            }

            break;
        }
    }
}

$("#mainCategorySelect").on("change", function () {
    addSubCategoryArray($(this).val());
});

$("#timeLimitBool").on("click", function () {
    toggleTimeInps($(this))
});

function toggleTimeInps(checkbox){
    $("#fullTimeInp").prop("disabled", !checkbox.is(":checked"));
    $("#questTimeInp").prop("disabled", !checkbox.is(":checked"));
}

$("#visibilitySelect").on("change", function(){
    if($(this).val() === "Group"){
        $("#chooseGroup").css("display", "block");
        getGroups();
    } else {
        $("#chooseGroup").css("display", "none");
    }
});


$("#groupSelect").on("change", function(){
    if($(this).val() === "Create new group"){
        startCreateGroup();
    }
});

$("#createQuiz").on("click", function(){

    //name, visibility_status, language, sub_category_id, description, time_limit
    var quizData = {};

    quizData.name = $("#quizName").val();
    quizData.visibility_status = parseInt($("#s_" + $("#visibilitySelect").val().split(" ")[0]).data("id"));
    quizData.language = parseInt($("#s_" + $("#languageSelect").val().split(" ")[0]).data("id"));
    quizData.sub_category_id = parseInt($("#s_" +  $("#subCategorySelect").val().split(" ")[0]).data("id"));
    quizData.description = $("#quizDesc").val();
    quizData.time_limit = $("#timeLimitBool").is(":checked") ? parseInt($("#fullTimeInp").val()) : null;

    console.log(quizData);

    $.ajax({
       url: quizHandlerUrl,
       method: "POST",
       data: {
           action: action,
           quizData: JSON.stringify(quizData)
       }
    }).done(function(response){
        console.log(response);

        response = JSON.parse(response);
        swal({
            title: response.title,
            type: response.type,
            text: response.text
        }).catch(swal.noop);

    });

});

//FUNCTIONS FOR SETTING THE SELECTORS TO RIGHT VALUES ON EDITPROJECT

function setCategoriesFromId(id) {
    for (var i = 1; i < allCategoryProperties.length; i++) {
        var subCategoryArray = categories[allCategoryProperties[i]];

        for (var j = 0; j < subCategoryArray.length; j++) {
            if (subCategoryArray[j].id == id) {
                var mainCategorySelect = $("#mainCategorySelect");
                mainCategorySelect.val(allCategoryProperties[i]);
                addSubCategoryArray(mainCategorySelect.val());
                $("#subCategorySelect").val(subCategoryArray[j].name);
                break;
            }
        }
    }
}