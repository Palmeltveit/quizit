<nav class="navbar fixed-top navbar-default navbar-expand-lg qNav">
    <a class="navbar-brand" href="index.php">QuizIt!</a>

    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="icon-bar top-bar"></span>
        <span class="icon-bar middle-bar"></span>
        <span class="icon-bar bottom-bar"></span>
        <span class="sr-only">Toggle navigation</span>
    </button>

    <div class="collapse navbar-collapse mainNav" id="navbarSupportedContent">
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
            <button class="btn proBtn mobile-hidden" type="submit">Search</button>
        </form>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item" style="text-align: center;;">
                <a class="nav-link" href="#" style="color: white">Categories</a>
            </li>
            <li class="nav-item" style="text-align: center">
                <?php if(isset($_SESSION['user_id'])){ ?>
                    <a class="nav-link" onclick="logout()">Log out</a>
                    <?php
                } else { ?>
                    <a class="nav-link" onclick="openAuthForm()">Log in / register</a>
                <?php } ?>
            </li>
        </ul>
    </div>
</nav>
<div class="container" style="padding-top: 60px;">