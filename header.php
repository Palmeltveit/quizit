<!DOCTYPE html>
<head>
    <title><?php echo $title; ?></title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="styles/imageUpload.css" />
    <link rel="stylesheet" href="styles/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="styles/bootstrap-grid.css" type="text/css">
    <link rel="stylesheet" href="styles/bootstrap-reboot.css" type="text/css">
    <link rel="stylesheet" href="styles/icons.css" type="text/css">
    <link rel="stylesheet" href="styles/dropzone.css" type="text/css">
    <link rel="stylesheet" href="styles/style.css" type="text/css">
    <link rel="stylesheet" href="styles/proNavToggle.css" type="text/css">
    <link rel="stylesheet" href="styles/authSheet.css" type="text/css">
    <script type="text/javascript" async src="js/MathJax-2.7.1/MathJax.js?config=TeX-MML-AM_CHTML"></script>
</head>
<body>
<?php

$sectionScripts = '';

?>

