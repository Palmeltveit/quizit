<?php

if(session_status() == PHP_SESSION_NONE) {
    session_start();
}

$response = array (
    "title" => "Failed to register",
    "text" => "please try again later",
    "type" => "error"
);

if(isset($_POST)){

    $authData = json_decode($_POST['authData']);

    include_once "../classes/UserManager.php";
    $userManager = new UserManager();

    if($userManager->checkEmailUniqueness($authData->email)){
        $userData = $userManager->registerUser($authData);

        if(!is_null($userData) && $userData){
            $response['title'] = "Register successful!";
            $response['text'] = "";
            $response['type'] = "success";
        }
    } else {
        $response['text'] = "email already in use";
    }

}

echo json_encode($response);