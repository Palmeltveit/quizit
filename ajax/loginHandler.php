<?php

if(session_status() == PHP_SESSION_NONE) {
    session_start();
}

$response = array (
    "title" => "Failed to log in",
    "text" => "please try again later",
    "type" => "error"
);

if(isset($_POST)){

    $authData = json_decode($_POST['authData']);

    if($authData->logout){
        unset($_SESSION['user_id']);
        unset($_SESSION['user_email']);
        unset($_SESSION['user_name']);
        session_destroy();

        $response['text'] = "successfully logged out!";
        $response['type'] = "success";
        $response['title'] = "";

    } else {

        include_once "../classes/UserManager.php";
        $userManager = new UserManager();

        if ($userData = $userManager->getUser($authData)) {
            $response['title'] = "Log in successful!";
            $response['text'] = "You are logged in as " . $userData['username'];
            $response['type'] = "success";

            $_SESSION['user_id'] = $userData['id'];
            $_SESSION['user_email'] = $userData['email'];
            $_SESSION['user_name'] = $userData['username'];
        }
    }
}

echo json_encode($response);