<?php

if(session_status() == PHP_SESSION_NONE) {
    session_start();
}

if(!isset($_SESSION['user_id'])) { exit; }

$response = [];

if(isset($_GET['action'])){

    $action = $_GET['action'];
    include_once "../classes/UserManager.php";
    $userManager = new userManager();

    switch ($action){

        case("get_groups"):{
            $response = $userManager->getUserGroups($_SESSION['user_id']);
            break;
        }

    }

} else if(isset($_POST['action'])){

    $action = $_POST['action'];
    include_once "../classes/UserManager.php";
    $userManager = new userManager();

    switch($action){

        case("create_group"):{
            break;
        }

    }

}

echo json_encode($response);