<?php

if(session_status() == PHP_SESSION_NONE) {
    session_start();
}

$response = [];

if(isset($_GET['action'])){

    $action = $_GET['action'];
    include_once "../classes/QuizManager.php";
    $quizManager = new QuizManager();

    switch ($action){

        case("get_categories"):{
            $response = $quizManager->getCategories();
            break;
        }

        case("edit_quiz"):{
            if(isset($_SESSION['user_id']) && isset($_GET['id'])) {
                $response = $quizManager->getQuiz($_GET['id'], $_SESSION['user_id']);
            }
            break;
        }
    }

} else if(isset($_SESSION['user_id']) && isset($_POST['action'])){

    $action = $_POST['action'];
    include_once "../classes/QuizManager.php";
    $quizManager = new QuizManager();

    switch($action){

        case("create"):{
            if(isset($_POST['quizData'])){
                $quizData = json_decode($_POST['quizData']);
                $quizData->owner_id = $_SESSION['user_id'];

                if($quizManager->createQuiz($quizData)){
                    $response = successResponse("Quiz created!");
                } else {
                    $response = errorResponse("Failed to create quiz", "Please try again later");
                }
            }
            break;
        }

    }
}

function successResponse($titleString, $text=""){
    return array(
        "title" => $titleString,
        "type" => "success",
        "text" => $text
    );
}

function errorResponse($titleString, $text=""){
    return array(
        "title" => $titleString,
        "type" => "error",
        "text" => $text
    );
}

echo json_encode($response);