<?php

if(session_status() == PHP_SESSION_NONE){
    session_start();
}

if(!isset($_SESSION['user_id'])){
    header("Location: index.php");
}

include_once "classes/QuizManager.php";
$quizManager = new QuizManager();

$quiz = $quizManager->getQuiz($_GET['id'], $_SESSION['user_id']);
$questions = $quizManager->getAllQuestions($quiz['id']);

$title = $quiz['name'];

include "header.php";
include "nav.php";

?>

<h2 style="text-align: center; padding-top: 20px;"><?= $quiz['name'] ?></h2><hr/>
<div class="container" style="">
    <div class="row">
        <div class="col-md-7">
            <div style="display: flex; justify-content: space-between; padding: 10px; border-bottom: 1px solid; border-radius: 2px;">
                <span style="font-size: smaller; text-align: left; padding: 5px;">Visible to: <b style="font-size: medium"><?= $quiz['status_name'] ?></b></span>
                <span style="font-size: smaller; text-align: center; padding: 5px;">Category: <b style="font-size: medium"><?= $quiz['sub_category_name'] ?></b></span>
                <span style="font-size: smaller; text-align: right; padding: 5px;">Language: <b style="font-size: medium"><?= $quiz['language_name'] ?></b></span>
            </div>
            <p style="border: 1px solid; margin-top: 10px; padding: 10px; border-radius: 5px;"><?= $quiz['description'] ?></p>
            <a href="createQuiz.php?action=edit&qId=<?= $quiz['id'] ?>"><button class="btn proBtn" style="width: 100%; margin: 0;">Edit Quiz</button></a>
            <a href=""><button class="btn proBtn" style="width: 100%; margin-left: 0; margin-right: 0; margin-top: 10px;">Play Quiz</button></a>
        </div>
        <div class="col-md-5" style="">
            <div class="col-md-12" style="padding: 0; border: 1px solid; border-radius: 5px;">
                <div style="width: 100%; display: flex; justify-content: space-between; align-items: center; padding: 10px; border-bottom: 2px solid;">
                    <h4>Questions</h4>
                    <a><button class="btn proBtn">Add Question</button></a>
                </div>
                <div style="min-height: 300px; max-height: 600px; overflow-y: scroll; margin-bottom: 5px;">

                </div>
            </div>
        </div>
    </div>
</div>
<?php
$sectionScripts = '
<script type="text/javascript" src="js/createGroup.js"></script>
<script type="text/javascript" src="js/createQuiz.js"></script>';

include "footer.php";

?>

