<?php

if(session_status() == PHP_SESSION_NONE){
    session_start();
}

$title = "Home";

include "header.php";
include "nav.php";

?>
<a href="createQuiz.php?action=create"><button class="btn btn-primary">Create Quiz</button></a>
<h2>Welcome back, <?= isset($_SESSION['user_name']) ? $_SESSION['user_name'] : "" ?>!</h2>
<p style="text-align: center">
    Integrer funksjonen: $$ f(x)= \int 3 \; \sqrt{5 \; x^{2}} + 9\,\mathrm{d}x $$
</p>

<?php
$sectionScripts = '';

include "footer.php";

?>

