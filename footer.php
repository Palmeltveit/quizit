        <script src="js/jquery-3.2.1.js"></script>
        <script src="https://unpkg.com/popper.js@1.12.5/dist/umd/popper.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js"></script>
        <script src="js/dropzone.js"></script>
        <script src="js/urlParameter.js"></script>
        <script src="js/auth.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.css" />
        <?php if(isset($sectionScripts)) {
            echo $sectionScripts;
        } ?>
    </div>
</body>
