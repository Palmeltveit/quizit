<?php

if(session_status() == PHP_SESSION_NONE){
    session_start();
}

if(!isset($_SESSION['user_id'])){
    header("Location: index.php");
}
$title = isset($_GET['qId']) ? "Edit Quiz" : "Create a new Quiz!";

include_once "classes/QuizManager.php";
$quizManager = new QuizManager();
$languages = $quizManager->getAll(DBManager::LANGUAGES);
$visibilityStatuses = $quizManager->getAll(DBManager::VISIBILITY_STATUSES);

include "header.php";
include "nav.php";

?>


<h2 id="headline" style="text-align: center; padding-bottom: 20px; padding-top: 20px;">Create a new Quiz!</h2>
<div class="container" style="">
    <div class="row">
        <div class="col-md-7">
            <div class="form-group" style="">
                <label for="quizName">Quiz Name</label>
                <input id="quizName" type="text" class="form-control">
            </div>
            <div class="form-group descriptionInp" style="">
                <label style="padding: 0; margin: 0;" for="quizDesc">Description / into text</label>
                <textarea style="min-height: 103px;" id="quizDesc" type="text" class="form-control"></textarea>
            </div>
        </div>
        <div class="col-md-5" style="margin: 0;">
            <div id="my-imageupload" class="imageupload" style="height: 100%;">
                <div class="card-header clearfix">
                    <h5 class="card-title pull-left">Upload cover photo</h5>
                    <div class="btn-group pull-right">
                        <button type="button" class="btn active">File</button>
                        <button type="button" class="btn">URL</button>
                    </div>
                </div>
                <div class="file-tab card-block" style="height: 100%">
                    <!--<div style="padding-top: 20px; display: flex; align-items: center; justify-content: center">
                    </div>-->
                    <form action="/" class="dropzone dz-clickable" id="my-awesome-dropzone">
                        <div class="dz-default dz-message">
                            <span>Drop files here to upload</span>
                        </div>
                    </form>
                </div>
                <div class="url-tab card-block">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Image URL">
                        <div class="input-group-btn">
                            <button type="button" class="btn">Submit</button>
                        </div>
                    </div>
                    <button type="button" class="btn">Remove</button>
                    <!-- The URL is stored here. -->
                    <input type="hidden" name="image-url">
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 40px;">
        <div class="col-md-4">
            <div class="col-md-12" style="padding: 0;">
                <label style="" for="languageSelect">Select Language</label>
                <select class="form-control" name="languageSelect" id="languageSelect">
                    <?php foreach($languages as $language => $value){ ?>
                        <option value="<?= $value['language'] ?>" id="s_<?= $value['language'] ?>" data-id="<?= $value['id'] ?>"><?= $value['language'] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-12 underDiv form-group" style="padding: 0">
                <input id="timeLimitBool" type="checkbox"/><label for="timeLimitBool">&nbsp; Time limit</label>
                <a id="timeLimitTip" data-toggle="tooltip" data-placement="top" title="Time limit can be set either as time per question, or for the full quiz. If you are unsure of the length of the quiz, you can leave blank and alter later.">
                    <i style="margin-left: 10px;" class='fa fa-question-circle'></i>
                </a>
                <div class="form-control" style="padding-left: 1px; padding-top: 1px;">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#timeFullQuiz" role="tab" aria-controls="home" aria-expanded="true">Full quiz</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#timePerQuestion" role="tab" aria-controls="profile">Per question</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent" style="margin-top: 10px;">
                        <div class="tab-pane fade show active" id="timeFullQuiz" role="tabpanel" aria-labelledby="home-tab">
                            <input disabled id="fullTimeInp" class="form-control" style="width: 60%; display: inline" type="number" value="20">
                            <label for="fullTimeInp">&nbsp;Minutes</label>
                        </div>
                        <div class="tab-pane fade" id="timePerQuestion" role="tabpanel" aria-labelledby="profile-tab">
                            <input disabled id="questTimeInp" class="form-control" style="width: 60%; display: inline" type="number" value="60">
                            <label for="questTimeInp">&nbsp;Seconds</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group col-md-4" style="">
            <div class="col-md-12" style="padding: 0;">
                <label style="" for="mainCategorySelect">Main category</label>
                <select class="form-control" name="quizVisibility" id="mainCategorySelect">
                </select>
            </div>
            <div class="form-group col-md-12 underDiv" style="padding: 0;">
                <label style="" for="subCategorySelect">Sub-category</label>
                <select class="form-control" name="subCategorySelector" id="subCategorySelect">
                </select>
            </div>
        </div>
        <div class="form-group  col-md-4" style="">
            <div class="col-md-12" style="padding: 0;">
                <label style="" for="visibilitySelect">Who can play this Quiz?</label>
                <select class="form-control" name="quizVisibility" id="visibilitySelect">
                    <?php foreach ($visibilityStatuses as $status => $value){ ?>
                        <option value="<?= $value['status_name'] ?>" id="s_<?= preg_split("/\s+/", $value['status_name'])[0] ?>" data-id="<?= $value['id'] ?>"><?= $value['status_name'] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-12 underDiv" style="display: none; padding: 0;" id="chooseGroup">
                <label for="groupSelect">Choose group</label>
                <select class="form-control" name="groupSelector" id="groupSelect">
                </select>
            </div>
        </div>
        <div class="col-md-12">
            <button class="btn proBtn" id="createQuiz" style="width: 100%; margin-bottom: 20px;">Create Quiz!</button>
        </div>
    </div>
</div>


<?php
$sectionScripts = '
<script type="text/javascript" src="js/createGroup.js"></script>
<script type="text/javascript" src="js/createQuiz.js"></script>';

include "footer.php";

?>

