<?php

class UserManager{

    private $dbManager = null;

    public function __construct()
    {
        include_once 'DBManager.php';
        $this->dbManager = DBManager::getInstance();
    }

    //GET FUNCTIONS
    public function getUser($authData){

        $query = "SELECT * FROM " . DBManager::USERS . " WHERE email=? AND password=?";
        if($prepStatement = $this->dbManager->prepare($query)) {

            $prepStatement->bind_param("ss", $authData->email, $authData->password);

            if ($prepStatement->execute()) {
                $returnUser = $prepStatement->get_result()->fetch_array();
                $prepStatement->close();

                return $returnUser;
            }
        }

        $prepStatement->close();
        return null;
    }

    public function getUserGroups($id){

        $response = [];

        $query = "SELECT " . DBManager::GROUP_MEMBERSHIPS . ".group_id, " . DBManager::GROUPS . ".* 
        FROM " . DBManager::GROUP_MEMBERSHIPS . " INNER JOIN " . DBManager::GROUPS . " 
        ON " . DBManager::GROUP_MEMBERSHIPS . ".group_id = " . DBManager::GROUPS . ".id 
        WHERE " . DBManager::GROUP_MEMBERSHIPS . ".user_id = {$id}";

        $queryResponse = $this->dbManager->runQuery($query);

        if($queryResponse){
            while($row = $this->dbManager->fetchAssoc($queryResponse)) {
                array_push($response, $row);
            }
        }

        $queryResponse->close();

        return $response;
    }

    //CREATE/ADD FUNCTIONS
    public function registerUser($userData){

        $query = "INSERT INTO ". DBManager::USERS . " (username, email, password) VALUES (?, ?, ?)";
        $prepStatement =$this->dbManager->prepare($query);

        $prepStatement->bind_param("sss",
            $this->dbManager->clean($userData->username),
            $this->dbManager->clean($userData->email),
            $this->dbManager->clean($userData->password)
        );

        $works = $prepStatement->execute();
        $prepStatement->close();


        return $works;
    }


    //CHECKING FUNCTIONS
    public function checkEmailUniqueness($email){
        $chechUniqueStatement = "SELECT email FROM " . DBManager::USERS . " WHERE email=?";
        $checkUniqueprepStatement = $this->dbManager->prepare($chechUniqueStatement);
        $checkUniqueprepStatement->bind_param("s", $email);
        $checkUniqueWorks = $checkUniqueprepStatement->execute();
        $checkUniqueprepStatement->store_result();
        if($checkUniqueWorks) {
            $returnVal = $checkUniqueprepStatement->num_rows == 0;
            $checkUniqueprepStatement->close();
            return ($returnVal);
        }

        $checkUniqueprepStatement->close();
        return null;
    }

}