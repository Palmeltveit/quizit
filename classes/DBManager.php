<?php
/**
 * Created by PhpStorm.
 * User: Paal
 * Date: 08.10.2017
 * Time: 13:10
 */

//singleton design

class DBManager
{
    const DBHOST = 'localhost';
    const DBUSER = 'root';
    const DBPASS = '';
    const DBNAME = 'QuizIt';

    const USERS = "users";
    const USER_FRIENDSHIPS = "user_friendships";
    const GROUPS = "groups";
    const GROUP_MEMBERSHIPS = "group_memberships";

    const QUIZZES = "quizzes";
    const QUIZ_IMAGES = "quiz_images";
    const QUESTIONS = "questions";
    const QUESTION_IMAGES = "question_images";
    const ALTERNATIVES = "alternatives";

    const VISIBILITY_STATUSES = "visibility_statuses";
    const CATEGORIES = "categories";
    const LANGUAGES = "languages";
    const SUB_CATEGORIES = "sub_categories";
    const RATINGS = "ratings";

    private $connection = null;

    public static $instance;

    private function __construct()
    {
        $this->connection = mysqli_connect($this::DBHOST, $this::DBUSER, $this::DBPASS, $this::DBNAME)
        or die("failed to connect to db: " . mysqli_connect_error());
    }

    public function __destruct()
    {
        mysqli_close($this->connection);
    }

    public function prepare($query){
        return mysqli_prepare($this->connection, $query);
    }

    public function runQuery($query){
        return mysqli_query($this->connection, $query);
    }

    public function fetchAssoc($queryResponse){
        return mysqli_fetch_assoc($queryResponse);
    }

    public function getLastError(){
        return mysqli_error($this->connection);
    }

    public function clean($string)
    {
        $string = filter_var($string, FILTER_SANITIZE_SPECIAL_CHARS);
        return mysqli_real_escape_string($this->connection, $string);
    }

    public static function getInstance(){
        if(!isset(DBManager::$instance)) {
            DBManager::$instance = new DBManager();
        }
        return DBManager::$instance;
    }

}