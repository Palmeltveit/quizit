<?php
/**
 * Created by PhpStorm.
 * User: Paal
 * Date: 10.10.2017
 * Time: 22:32
 */

class QuizManager
{
    private $dbManager = null;

    public function __construct()
    {
        include_once 'DBManager.php';
        $this->dbManager = DBManager::getInstance();
    }


    //GET FUNCTIONS
    public function getQuiz($id, $userId){
        $response = [];

        $query = "SELECT " . DBManager::QUIZZES . ".*, " .
            DBManager::VISIBILITY_STATUSES . ".status_name, " .
            DBManager::SUB_CATEGORIES . ".name AS sub_category_name, " .
            DBManager::LANGUAGES . ".language AS language_name FROM " . DBManager::QUIZZES . " " .
            "INNER JOIN " . DBManager::VISIBILITY_STATUSES . " ON " . DBManager::VISIBILITY_STATUSES . ".id = " . DBManager::QUIZZES . ".visibility_status " .
            "INNER JOIN " . DBManager::SUB_CATEGORIES . " ON " . DBManager::SUB_CATEGORIES . ".id = " . DBManager::QUIZZES . ".sub_category_id " .
            "INNER JOIN " . DBManager::LANGUAGES . " ON " . DBManager::LANGUAGES . ".id = " . DBManager::QUIZZES . ".language " .
            "WHERE " . DBManager::QUIZZES . ".id=? AND " . DBManager::QUIZZES . ".owner_id=?";

        $preparedStatement = $this->dbManager->prepare($query);

        $preparedStatement->bind_param("ii", $id, $userId);

        $works = $preparedStatement->execute();
        if($works){
            $response = $preparedStatement->get_result()->fetch_array();
        }
        $preparedStatement->close();

        return $response;
    }

    public function getCategories(){
        $response = array(
            "main_categories" => []
        );

        $query = "SELECT * FROM " . DBManager::CATEGORIES;
        $queryResponse = $this->dbManager->runQuery($query);

        if($queryResponse) {

            while($row = $this->dbManager->fetchAssoc($queryResponse)) {
                array_push($response["main_categories"], $row);

                $subQuery = "SELECT * FROM " . DBManager::SUB_CATEGORIES . " WHERE over_category={$row['id']}";
                $subQueryResponse = $this->dbManager->runQuery($subQuery);

                if($subQueryResponse){

                    $rowName = $row['name'];
                    $response[$rowName] = [];

                    while($subRow = $this->dbManager->fetchAssoc($subQueryResponse)){
                        array_push($response[$rowName], $subRow);
                    }
                }

                $subQueryResponse->close();
            }
        }
        $queryResponse->close();

        return $response;
    }

    public function getAll($tableName){
        $response = [];

        $query = "SELECT * FROM " . $tableName;
        $queryResponse = $this->dbManager->runQuery($query);

        if($queryResponse){
            while($row = $this->dbManager->fetchAssoc($queryResponse)){
                array_push($response, $row);
            }
        }

        return $response;
    }

    function getAllQuestions($quizId){
        $response = [];

        $query = "SELECT * FROM " . DBManager::QUESTIONS . " WHERE quiz_id={$quizId}";
        $queryResponse = $this->dbManager->runQuery($query);

        if($queryResponse){
            while($row = $this->dbManager->fetchAssoc($queryResponse)){
                array_push($response, $row);
            }
        }

        return $response;
    }

    //ADD / CREATE FUNCTIONS
    public function createQuiz($quizData){

        $query = "INSERT INTO " . DBManager::QUIZZES . "(name, visibility_status, language, owner_id, sub_category_id, description, time_limit) VALUES (?,?,?,?,?,?,?)";
        $preparedStatement = $this->dbManager->prepare($query);

        $preparedStatement->bind_param("siiiisi",
            $this->dbManager->clean($quizData->name),
            $this->dbManager->clean($quizData->visibility_status),
            $this->dbManager->clean($quizData->language),
            $this->dbManager->clean($quizData->owner_id),
            $this->dbManager->clean($quizData->sub_category_id),
            $this->dbManager->clean($quizData->description),
            $this->dbManager->clean($quizData->time_limit)
        );

        $works = $preparedStatement->execute();
        $preparedStatement->close();

        return $works;

    }

    public function addQuestions($questions){
        $query = "INSERT INTO " . DBManager::QUESTIONS . "(quiz_id, title, description, explanation, time_limit, latex_support, NOCA) VALUES(?,?,?,?,?,?,?)";
        $preparedStatement = $this->dbManager->prepare($query);

        $preparedStatement->bind_param("isssibi", $quizId, $title, $description, $explanation, $timeLimit, $latexSupport, $noca);

        $works = true;

        foreach($questions as $question => $value){

            $quizId = $this->dbManager->clean($value->quizId);
            $title = $this->dbManager->clean($value->title);
            $description = $this->dbManager->clean($value->description);
            $explanation = $this->dbManager->clean($value->explanation);
            $timeLimit = $this->dbManager->clean($value->timeLimit);
            $latexSupport = $this->dbManager->clean($value->latexSupport);
            $noca = $this->dbManager->clean($value->noca);

            if(!$preparedStatement->execute()){
                $works = false;
                break;
            }
        }

        $preparedStatement->close();
        return $works;

    }

    public function addQuestionAlternatives($alternatives){

    }

}