CREATE TABLE users(
  id int(11) AUTO_INCREMENT,
  username varchar(60) UNIQUE NOT NULL,
  email varchar(60) UNIQUE NOT NULL,
  password varchar(60) NOT NULL,
  profile_pic varchar(512) DEFAULT NULL,
  level int(11) DEFAULT 1,
  PRIMARY KEY (id)
);

CREATE TABLE languages(
  id int(11) AUTO_INCREMENT,
  language varchar(60) NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO languages(language) VALUES
  ('English'),
  ('Norsk');

CREATE TABLE visibility_statuses(
  id int(11) AUTO_INCREMENT,
  status_name varchar(60) NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO visibility_statuses(status_name) VALUES
  ('Everyone'),
  ('Friends'),
  ('Group'),
  ('Just me');

CREATE TABLE categories(
  id int(11) AUTO_INCREMENT,
  name varchar(60) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE sub_categories(
  id int(11) AUTO_INCREMENT,
  over_category int(11),
  name varchar(60) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (over_category) REFERENCES categories(id)
);

CREATE TABLE quizzes(
  id int(11) AUTO_INCREMENT,
  name varchar(128) NOT NULL,
  visibility_status int(11) DEFAULT 1,
  language int(11) DEFAULT 1,
  minus_on_wrong bit(1) DEFAULT 0,
  rating decimal(11, 3) DEFAULT NULL,
  owner_id int(11) NOT NULL,
  sub_category_id int(11) NOT NULL,
  description varchar(1024) DEFAULT NULL,
  time_limit int(11) DEFAULT NULL,
  questions int(11) DEFAULT 0,
  -- kan kanskje fjerne questions i favør for COUNT questions WHERE id=quiz.id
  PRIMARY KEY (id),
  FOREIGN KEY (owner_id) REFERENCES users(id),
  FOREIGN KEY (sub_category_id) REFERENCES sub_categories(id),
  FOREIGN KEY (visibility_status) REFERENCES visibility_statuses(id),
  FOREIGN KEY (language) REFERENCES languages(id)
);

CREATE TABLE quiz_images(
  quiz_id int(11) NOT NULL,
  path varchar(512) NOT NULL,
  FOREIGN KEY (quiz_id) REFERENCES quizzes(id)
);

CREATE TABLE questions(
  id int(11) AUTO_INCREMENT,
  quiz_id int(11) NOT NULL,
  title varchar(128) NOT NULL,
  description varchar(512) DEFAULT NULL,
  explanation varchar(2048) DEFAULT NULL,
  time_limit int(11) DEFAULT NULL,
  latex_support bit(1) DEFAULT 0,
  NOCA int(11) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (quiz_id) REFERENCES quizzes(id)
);

CREATE TABLE question_images(
  question_id int(11) NOT NULL,
  path varchar(512) NOT NULL,
  FOREIGN KEY (question_id) REFERENCES questions(id)
);

-- NOCA = NUMBER OF CORRECT ALTERNATIVES

CREATE TABLE alternatives(
  question_id int(11) NOT NULL,
  answer varchar(128) NOT NULL,
  correct bit(1) DEFAULT 0,
  FOREIGN KEY (question_id) REFERENCES questions(id)
);

INSERT INTO categories(name) VALUES ('School'), ('Trivia');

INSERT INTO sub_categories(over_category, name) VALUES
  (1, 'Math'),
  (1, 'Physics'),
  (1, 'Chemistry'),
  (1, 'Religion'),
  (1, 'Literature'),
  (1, 'History'),
  (2, 'Movies'),
  (2, 'TV'),
  (2, 'Cars'),
  (2, 'Technology'),
  (2, 'History');

CREATE TABLE ratings(
  user_id int(11) NOT NULL,
  quiz_id int(11) NOT NULL,
  rating int(11) NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (quiz_id) REFERENCES quizzes(id)
);

CREATE TABLE user_friendships(
  user_id_1 int(11) NOT NULL,
  user_id_2 int(11) NOT NULL,
  accepted bit(1) DEFAULT 0,
  FOREIGN KEY (user_id_1) REFERENCES users(id),
  FOREIGN KEY (user_id_2) REFERENCES users(id)
);

CREATE TABLE groups(
  id int(11) AUTO_INCREMENT,
  name varchar(60) NOT NULL,
  owner_id int(11) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (owner_id) REFERENCES users(id)
);

CREATE TABLE group_roles(
  id int(11) AUTO_INCREMENT,
  role_name varchar(60) NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO group_roles(role_name) VALUES
  ('member'),
  ('moderator');

CREATE TABLE group_memberships(
  group_id int(11) NOT NULL,
  user_id int(11) NOT NULL,
  role int(11) DEFAULT 1,
  FOREIGN KEY (group_id) REFERENCES groups(id),
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (role) REFERENCES group_roles(id)
);
